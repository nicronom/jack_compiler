import Parser.*;

import java.io.File;
import java.util.ArrayList;

/**
 * Temporary class to test lexer functionality.
 * Given a valid directory of Jack source files, outputs all tokens
 * of the input streams. Helpful messages when errors are caught either
 * with invalid dir supplied or mistake encountered in a source file is displayed.
 * */

public class Main{
    public static void main(String args[]) {

        // no cla supplied
        if (args.length == 0 || args == null){
            System.out.println("No source file directory has been supplied for the compiler");
            return;
        }

        // container for all file paths in the supplied directory
        ArrayList<String> filePaths = new ArrayList<String>();
        // the parent folder supplied
        File folder = new File(args[0]);
        // all containing file descriptors
        File[] fileList = folder.listFiles();
        try{
          for (int i=0; i < fileList.length; i++) {
              // we only need to extract source files ending with .jack
              if (fileList[i].isFile() && fileList[i].getName().endsWith(".jack")) {
                  filePaths.add(args[0] + fileList[i].getName());
              }
          }
        }
        catch (Exception e) {
          System.out.println("Directory supplied " + args[0] + " is invalid");
          System.exit(0);
        }

        if (fileList.length == 0) {
            System.out.println("Directory supplied " + args[0] + " is invalid");
            System.exit(0);
        }

        if (filePaths.size() == 0) {
          System.out.println("No jack source files found in supplied directory " + args[0]);
          System.exit(0);
        }

        Parser parser = new Parser(filePaths);
        parser.Init();

    }
}
