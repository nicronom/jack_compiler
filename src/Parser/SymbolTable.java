package Parser;

import java.util.LinkedList;

/** Generic table to keep able to track the symbols of both
 *  class and method scope is defined below. */
public class SymbolTable {
    private LinkedList<Symbol> symbols;
    // a counter for static if class table OR for arg in method table
    private int count1;
    // a counter for field if class table OR for var in method table
    private int count2;

    public SymbolTable() {
        symbols = new LinkedList<Symbol>();
        count1 = 0;
        count2 = 0;
    }

    // inserts a symbol to the table of symbols
    public void insert(Symbol s) {
        // handles assigning addresses to symbols
        if (s.kind.equals("static") || s.kind.equals("arg")) {
            // the address of a static or arg symbol is value of count1
            s.localAddress = count1;
            // increments the respective address for next record
            count1++;
        }
        if (s.kind.equals("field") || s.kind.equals("var")) {
            s.localAddress = count2;
            count2++;
        }
        if (s.kind.equals("type") || s.kind.equals("constructor")
                || s.kind.equals("function") || s.kind.equals("method")
                || s.kind.equals("ret")) {
            // indicate that the address is not important
            s.localAddress = -1;
        }

        symbols.add(s);
    }

    // searches all symbols in the table for the given name identifier
    public Symbol lookup(String name) {
        for (Symbol s: symbols) {
            if (s.name.equals(name)) {
                return s;
            }
        }

        // null object if the symbol was not found
        return null;
    }

    public LinkedList<Symbol> getAllSymbols() {
        return symbols;
    }

    // prints information about all symbols in the table
    public void printTable() {
        System.out.println("\nPrinting symbol table\n");
        for (Symbol s: symbols) {
            System.out.println(s.toString());
        }
        System.out.println("Printing of symbol table is complete\n\n");
    }

    public int getStaticOrArgCount() {
        return this.count1;
    }

    public int getFieldOrVarCount() {
        return this.count2;
    }
}
