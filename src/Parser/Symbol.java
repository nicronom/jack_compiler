package Parser;

import java.util.LinkedList;

/** Generic symbol able to define all possible classified symbols of
 *  the Jack Programming Language is defined below. */
public class Symbol {
    // the name identifier of the symbol
    public String name;
    // the corresponding type of that symbol, i.e an elt of the set:
    // {simple, Array, String, static, idName, retType}
    public String type;
    // the kind is an element of the set:
    // {type, static, field, constructor, function, method, arg, var, ret}
    public String kind;
    // the offset address of the symbol based on its locality
    public int localAddress;
    // the table of symbols of a method, null if the kind is not a method
    public SymbolTable methodTable = null;
    // the value set to the variable symbol
    public boolean valueSet = false;

    public Symbol () {}

    // Formats the symbol to a descriptive string
    @Override
    public String toString() {
        String displayedAddress;
        // if the address is not relevant for code generation it is ommited
        if (localAddress == -1) {
            displayedAddress = "";
        }
        else {
            displayedAddress = String.valueOf(localAddress);
        }

        LinkedList<String> methodSymbols = new LinkedList<>();
        // each method also has a method symbol table for display
        if (kind.equals("method")
                || kind.equals("constructor")
                || kind.equals("function")) {
            for (Symbol s: methodTable.getAllSymbols()) {
                // similarly we need not display local address if irrelevant
                String localDisplayAddress;
                if (s.localAddress == -1) {
                    localDisplayAddress = "";
                }
                else {
                    localDisplayAddress = String.valueOf(s.localAddress);
                }
                // adds the description of each symbol in the method symbol table
                methodSymbols.add(s.name + " " + s.type + " " + s.kind + " " + localDisplayAddress);
            }
        }
        String symbolDescr = name + " " + type + " " + kind + " " + displayedAddress + "\n";
        // if there are method symbols, appends it to the string to be returned
        if (methodSymbols.size() != 0) {
            for (String s: methodSymbols) {
                symbolDescr = symbolDescr + s + "\n";
            }
        }

        return symbolDescr;
    }
}
