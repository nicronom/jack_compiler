package Parser;

import Lexer.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * This class implements the Parser component of the Jack compiler.
 * Given a jack source file the parser will attempt to initialise the
 * Lexer to extract the tokens of the input file. If for any reason that
 * process fails a message is displayed on the screen and the parsing is not
 * initialised. On successful token extraction, the parser goes through the
 * list of tokens and verifies that the structure of the source files follows
 * the grammar of the Jack language. In addition, the parser keeps track of
 * each declared classes' symbol tables, including the generic Jack class types
 * such as String, Array etc. Furthermore, at compile time,the Parser is
 * responsible for performing semantic checks on the available symbol tables.
 * After all checks have been performed, each relevant source line is translated
 * into vm assembly code of that jack source file. */
public class Parser {
    // to maintain a list of paths
    private ArrayList<String> filePaths;
    // the currently executing lexer object
    private Lexer lexer;
    // list of all class names encountered
    private ArrayList<String> classes;
    // the list of encountered symbols in all source files
    private ArrayList<SymbolTable> classTables;
    // the index of currently executing class
    private int atClass;
    // the global symbol table
    private SymbolTable globalTable;
    // the local table of the currently executing method
    private SymbolTable methodTable;
    // list of unresolved names <className, routineName>
    private LinkedHashMap<String, String> unresolved;
    // tracks line and filename for informative warning message
    private ArrayList<String> unresolvedMessage;
    // list of unresolved method call argument match check
    // <Routine.subroutine, List<args>>
    private LinkedHashMap<String, ArrayList<String>> unresolvedArgs;
    // tracks line and filename for informative warning message
    private ArrayList<String> unresolvedArgsMessage;
    // to reference what type should the curr method return
    private String methodRetType = null;
    // to reference the name of the current method context
    private String currMethodName = null;
    // list containing the arg types of the currently called method
    private ArrayList<String> callArgs;
    // the current expression type evaluation
    private String currExprType = null;
    // the current global subroutine declar name
    private String funcDeclarName = null;
    // the current global subroutine return type
    private String funcDeclarReturn = null;
    // the commands to be written to a vm exec file
    private LinkedList<String> codeCommands;
    // flags to check whereabouts of return statement
    private boolean inIf = false;
    private boolean inElse = false;
    private boolean notAllPaths = false;
    private boolean returnEncountered = false;

    // extracts the files in the directory and
    // initialises the internal structure of the parser
    public Parser(ArrayList<String> filePaths) {
        this.filePaths = filePaths;
        this.classes = new ArrayList<>();
        this.classTables = new ArrayList<SymbolTable>();
        // 0 classes found initially
        this.atClass = 0;
        this.codeCommands = new LinkedList<>();
        this.unresolved = new LinkedHashMap<>();
        this.unresolvedArgs = new LinkedHashMap<>();
        this.callArgs = new ArrayList<>();
        this.unresolvedMessage = new ArrayList<>();
        this.unresolvedArgsMessage = new ArrayList<>();
    }

    /* Attempts to extract all tokens of the input source file using the Lexer.
     * On failure signals the user and terminates. If successful attempts to parse
     * the tokens to determine if the overall structure follows the Jack programming
     * language grammar. Should at any given time an error is encountered, a useful
     * description of the error is given, including the line, near token and what token
     * was expected.*/
    public void Init() {

        // inserts the pre-defined jack class types' symbol tables
        insertJackClassDefinitions();
        // initialises parsing of each of the source files
        for (String file : filePaths) {
            try {
                // lexical analysis is made first
                this.lexer = new Lexer(file);
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("\nFailed to initialise lexer");
                return;
            }

            //System.out.println("\nLexer initialised successfully\n");

            // initialise the tables to populate the class' symbols with
            globalTable = new SymbolTable();
            // populates each global table and method table with generic
            // types of jack language, i.e. char, int, boolean.
            populateTypes(globalTable);
            methodTable = new SymbolTable();

            System.out.println("\nInitialising parsing of source file " + file + "\n");
            Parse();

            //globalTable.printTable();

            // finally add the parsed table to a list of class symbol talbes
            SymbolTable classTable = new SymbolTable();
            insertAllSymbols(globalTable, classTable);
            classTables.add(classTable);
            // we increment the counter in order to track where to
            // store next classes' symbol table and identifier.
            atClass++;
        }

        // after all source files have been compiler, unresolved methods
        // and types should be resolvable. A useful error message is
        // indicated otherwise.
        resolve();
        System.out.println("\nParsing was successful, no errors were detected. \n");
        // creates the appropriate vm files of the source files after successful parse
        formatCodeCommands();
        try {
            createVMFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* TEMP method to ensure that parsing is being done successfully
     * for debugging and verification purposes. */
    private void Ok(Token token) {
        //System.out.println(token.getLexeme() + " was ok");
    }

    // Method to display a useful message to the user when an error is encountered
    private void Error(Token token, String message) {
        System.out.println(
                "Error."
                + descriptiveMessage(token)
                + message);

        System.exit(0);
    }

    // Method to display a warning during semantic check.
    private void Warning(Token token, String message) {
        System.out.println(
                "WARNING!"
                + descriptiveMessage(token)
                + message);
    }

    // extracts a useful descriptive message of that token
    private String descriptiveMessage(Token token) {
        return " In line "
                + token.getLine()
                + ", close to \'"
                + token.getLexeme()
                + "\'.";
    }

    // the default provided jack class definitions are
    // inserted to the symbol tables
    private void insertJackClassDefinitions() {

        insertArrayClass();
        insertStringClass();
        insertKeyboardClass();
        insertOutputClass();
        insertMemoryClass();
        insertSysClass();
        insertScreenClass();

    }

    // Inserts the Jack Array class to the symbol tables along its
    // publically defined method declarations.
    private void insertArrayClass() {

        SymbolTable arrayTable = new SymbolTable();

        Symbol newFunc = createMethod("new",
                "function", "Array");
        insertArg(newFunc, "int", "size");

        arrayTable.insert(newFunc);

        Symbol disposeFunc = createMethod("dispose",
                "method", "Array");
        arrayTable.insert(disposeFunc);

        this.classes.add("Array");
        this.classTables.add(arrayTable);
        this.atClass++;
    }

    // Inserts the Jack Screen class to the symbol tables along its
    // publically defined method declarations.
    private void insertScreenClass() {
        SymbolTable screenTable = new SymbolTable();

        Symbol clearScreen = createMethod("clearScreen", "function", "void");
        screenTable.insert(clearScreen);

        Symbol setColor = createMethod("setColor", "function", "void");
        insertArg(setColor, "boolean", "b");
        screenTable.insert(setColor);

        Symbol drawPixel = createMethod("drawPixel", "function", "void");
        insertArg(drawPixel, "int", "x");
        insertArg(drawPixel, "int", "y");
        screenTable.insert(drawPixel);

        Symbol drawLine = createMethod("drawLine", "function", "void");
        insertArg(drawLine, "int", "x1");
        insertArg(drawLine, "int", "y1");
        insertArg(drawLine, "int", "x2");
        insertArg(drawLine, "int", "y2");
        screenTable.insert(drawLine);

        Symbol drawRectangle = createMethod("drawRectangle", "function", "void");
        insertArg(drawRectangle, "int", "x1");
        insertArg(drawRectangle, "int", "y1");
        insertArg(drawRectangle, "int", "x2");
        insertArg(drawRectangle, "int", "y2");
        screenTable.insert(drawRectangle);

        Symbol drawCircle = createMethod("drawCircle", "function", "void");
        insertArg(drawCircle, "int", "x");
        insertArg(drawCircle, "int", "y");
        insertArg(drawCircle, "int", "r");
        screenTable.insert(drawCircle);

        this.classes.add("Screen");
        this.classTables.add(screenTable);
        this.atClass++;
    }

    // Inserts the Jack Keyboard class to the symbol tables along its
    // publically defined method declarations.
    private void insertKeyboardClass() {
        SymbolTable keyboardTable = new SymbolTable();

        Symbol keyPressed = createMethod("keyPressed",
                "function", "char");
        keyboardTable.insert(keyPressed);

        Symbol readChar = createMethod("readChar",
                "function", "char");
        keyboardTable.insert(readChar);

        Symbol readLine = createMethod("readLine",
                "function", "String");
        insertArg(readLine, "String", "message");
        keyboardTable.insert(readLine);

        Symbol readInt = createMethod("readInt",
                "function", "int");
        insertArg(readInt, "String", "message");
        keyboardTable.insert(readInt);

        this.classes.add("Keyboard");
        this.classTables.add(keyboardTable);
        this.atClass++;
    }

    // Inserts the Jack Output class to the symbol tables along its
    // publically defined method declarations.
    private void insertOutputClass() {
        SymbolTable outputTable = new SymbolTable();

        Symbol init = createMethod("init", "function", "void");
        outputTable.insert(init);

        Symbol moveCursor = createMethod("moveCursor", "function", "void");
        insertArg(moveCursor, "int", "i");
        insertArg(moveCursor, "int", "j");
        outputTable.insert(moveCursor);

        Symbol printChar = createMethod("printChar", "function", "void");
        insertArg(printChar,"char","c");
        outputTable.insert(printChar);

        Symbol printString = createMethod("printString", "function", "void");
        insertArg(printString,"String","str");
        outputTable.insert(printString);

        Symbol printInt = createMethod("printInt", "function", "void");
        insertArg(printInt,"int","i");
        outputTable.insert(printInt);

        Symbol println = createMethod("println", "function", "void");
        outputTable.insert(println);

        Symbol backSpace = createMethod("backSpace", "function", "void");
        outputTable.insert(backSpace);

        this.classes.add("Output");
        this.classTables.add(outputTable);
        this.atClass++;
    }

    // Inserts the Jack Memory class to the symbol tables along its
    // publically defined method declarations.
    private void insertMemoryClass() {

        SymbolTable memoryTable = new SymbolTable();

        Symbol peek = createMethod("peek", "function", "int");
        insertArg(peek, "int", "address");
        memoryTable.insert(peek);

        Symbol poke = createMethod("poke", "function", "void");
        insertArg(poke, "int", "address");
        insertArg(poke, "int", "value");
        memoryTable.insert(poke);

        Symbol alloc = createMethod("alloc", "function", "Array");
        insertArg(alloc, "int", "size");
        memoryTable.insert(alloc);

        Symbol deAlloc = createMethod("deAlloc", "function", "void");
        insertArg(deAlloc, "Array", "o");
        memoryTable.insert(deAlloc);

        this.classes.add("Memory");
        this.classTables.add(memoryTable);
        this.atClass++;
    }

    // Inserts the Jack Sys class to the symbol tables along its
    // publically defined method declarations.
    private void insertSysClass() {
        SymbolTable sysTable = new SymbolTable();

        Symbol halt = createMethod("halt", "function", "void");
        sysTable.insert(halt);

        Symbol error = createMethod("error", "function", "void");
        insertArg(error, "int", "errorCode");
        sysTable.insert(error);

        Symbol wait = createMethod("wait", "function", "void");
        insertArg(wait, "int", "duration");
        sysTable.insert(wait);

        this.classes.add("Sys");
        this.classTables.add(sysTable);
        this.atClass++;
    }

    // Inserts the Jack String class to the symbol tables along its
    // publically defined method declarations.
    private void insertStringClass() {
        SymbolTable stringTable = new SymbolTable();

        Symbol newFunc = createMethod("new",
                "constructor", "String");
        insertArg(newFunc, "int", "length");
        stringTable.insert(newFunc);

        Symbol disposeFunc = createMethod("dispose", "method", "String");
        stringTable.insert(disposeFunc);

        Symbol length = createMethod("length", "method", "int");
        stringTable.insert(length);

        Symbol charAt = createMethod("charAt", "method", "char");
        insertArg(charAt, "int", "j");
        stringTable.insert(charAt);

        Symbol setCharAt = createMethod("setCharAt",  "method", "void");
        insertArg(setCharAt,"int", "j");
        insertArg(setCharAt, "char", "c");
        stringTable.insert(setCharAt);

        Symbol appendChar = createMethod("appendChar", "method", "String");
        insertArg(appendChar, "char", "c");
        stringTable.insert(appendChar);

        Symbol eraseLastChar = createMethod("eraseLastChar", "method", "void");
        stringTable.insert(eraseLastChar);

        Symbol intValue = createMethod("intValue", "method", "int");
        stringTable.insert(intValue);

        Symbol setInt = createMethod("setInt", "method", "void");
        insertArg(setInt, "int", "number");
        stringTable.insert(setInt);

        Symbol newLine = createMethod("newLine", "function", "void");
        stringTable.insert(newLine);

        Symbol backSpace = createMethod("backSpace", "function", "void");
        stringTable.insert(backSpace);

        Symbol doubleQuote = createMethod("doubleQuote", "function", "void");
        stringTable.insert(doubleQuote);

        this.classes.add("String");
        this.classTables.add(stringTable);
        this.atClass++;
    }

    // utlity method for initialising a method symbol and its this arg
    private Symbol createMethod(String funcName,
                                          String funcKind,
                                          String funcType) {
        Symbol method = new Symbol();
        method.name = funcName;
        method.kind = funcKind;
        method.type = funcType;
        method.methodTable = new SymbolTable();

        Symbol thisArg = new Symbol();
        thisArg.kind = "arg";
        thisArg.type = funcType;
        thisArg.name = "this";

        method.methodTable.insert(thisArg);
        return method;
    }

    // utility method to insert an argument to a method symbol' table
    private void insertArg(Symbol method, String type,
                             String name) {
        Symbol newArg = new Symbol();
        newArg.kind = "arg";
        newArg.type = type;
        newArg.name = name;

        method.methodTable.insert(newArg);
    }

    // determines the command expression of a type
    private String determineVarCommandKind(Symbol s) {
        String kind = s.kind;
        if (s.kind.equals("arg")) {
            kind = "argument " + s.localAddress;
        }
        if (s.kind.equals("var")) {
            kind = "local " + s.localAddress;
        }
        if (s.kind.equals("field")) {
            kind = "field " + s.localAddress;
        }

        return kind;
    }

    // Method to check the local scope table and throw an error if a symbol is redeclared
    private void checkSymbolIsUndeclared(Token token, SymbolTable locTable) {
        Symbol s = locTable.lookup(token.getLexeme());
        // if a symbol with that name already exists
        if (s != null ) {
            // if the previous symbol was a type declaration
            if (s.kind.equals("type")) {
                // alter message if the type is Jack pre-defined.
                if (s.name.equals("char")
                        || s.name.equals("int")
                        || s.name.equals("boolean")
                        || s.name.equals("Array")
                        || s.name.equals("String")) {
                    Warning(token, s.type + " is a reserved pre-defined Jack type.");
                }
                // the message if the type is user declared.
                Warning(token, "Type of that name is already declared in this scope.");
            }
            // otherwise it is a local redeclaration from a user
            Warning(token, "Local variable or method with this name is already declared in this scope");
        }
    }

    // populates the current table with Jack pre-defined types
    private void populateTypes(SymbolTable locTable) {
        // the character type symbol is defined below
        Symbol s = new Symbol();
        s.kind = "type";
        s.name = "char";
        s.type = "char";
        locTable.insert(s);

        // the int type
        Symbol sInt = new Symbol();
        sInt.kind = "type";
        sInt.name = "int";
        sInt.type = "int";
        locTable.insert(sInt);

        // the boolean type
        Symbol sBool = new Symbol();
        sBool.kind = "type";
        sBool.name = "boolean";
        sBool.type = "boolean";
        locTable.insert(sBool);
    }

    // utility method to insert all symbols from one table to another
    private void insertAllSymbols(SymbolTable from, SymbolTable to) {
        for (Symbol s: from.getAllSymbols()) {
            to.insert(s);
        }
    }

    // utility method to merge the current method table to the global
    // table by inserting the appropriate method symbol, whose method
    // table becomes the one we are trying to merge.
    private void mergeMethodTable(SymbolTable local, String methodName, String retType, String methodKind) {
        // the method description is added appropriately
        Symbol s = new Symbol();
        s.name = methodName;
        s.type = retType;
        s.kind = methodKind;
        // the table to be merged is method's symbol table
        s.methodTable = local;
        // performs check if return type of method matches the return found
        for (Symbol msymb: methodTable.getAllSymbols()) {

            if (msymb.kind.equals("ret")) {
                if (!msymb.type.equals(s.type)) {
                    // the constructor should return 'this' keyword,
                    // hence the types might be missmatched, checks for that case
                    if (!(methodKind.equals("constructor")
                            && msymb.type.equals("this"))) {
                                System.out.println(s.kind
                                        + " declared to return "
                                        + s.type
                                        + " but returned "
                                        + msymb.type
                                        + " in "
                                        + s.name
                                        + " of class "
                                        + classes.get(atClass));
                    }
                }
            }
        }
        // symbol is ready to be inserted to the global table
        globalTable.insert(s);
    }

    // Parses the list of extracted tokens in the source file and checks if it matches the grammar.
    private void Parse() {
        Token token;

        token = lexer.PeekNextToken();
        // until the end of file is encountered, at which point all extracted tokens were examined.
        while (token.getType() != TokenType.eof) {
            if (is(token,"class")) {
                classDeclar();
            }
            else {
                Error(token, "Expected class declaration got " + token.getLexeme());
            }

            token = lexer.PeekNextToken();
        }
    }

    // Determines if a valid class declaration is encountered.
    private void classDeclar() {

        Token token = lexer.GetNextToken();
        if (is(token, "class")) {
            Ok(token);

            token = lexer.GetNextToken();
            if (isIdent(token)) {
                Ok(token);
                // checks if a class is already declared with this name
                for (String className: classes) {
                    if (className.equals(token.getLexeme())) {
                        Warning(token, "Redeclaration encountered."
                                + "Class with that name is already defined.");
                    }
                }
                // otherwise adds the newly declared class to the list of class names
                classes.add(token.getLexeme());
                // line to identify the new .vm file break
                codeCommands.add("class " + token.getLexeme());

                token = lexer.GetNextToken();
                if (is(token,"{")) {
                    Ok(token);

                    token = lexer.PeekNextToken();
                    while (isBeginClassVarDeclar(token)
                            || isBeginSubroutineDeclar(token)) {

                        memberDeclar();

                        token = lexer.PeekNextToken();
                    }

                    token = lexer.GetNextToken();
                    if (is(token,"}")) {
                        Ok(token);
                    }
                    else {
                        Error(token, "Expected } got " + token.getLexeme());
                    }
                }
                else {
                    Error(token, "Expected { got " + token.getLexeme());
                }
            }
            else {
                Error(token, "Expected identifier got " + token.getType());
            }
        }
        else {
            Error(token, "Expected class got " + token.getLexeme());
        }
    }

    // Determines if a valid member declaration is encountered
    private void memberDeclar() {

        Token token = lexer.PeekNextToken();
        /*
        * It is the case when it is either a class variable declaration or
        * a subroutine declaration. Therefore peek the next token to check
        * if it matches the beginning of those, if not throw an error. */
        if (isBeginClassVarDeclar(token)) {
            classVarDeclar();
        }
        else if (isBeginSubroutineDeclar(token)) {
            subroutineDeclar();
        }
        else {
            Error(token, "Expected sub-class level declaration got " + token.getLexeme());
        }

    }

    // Determines if a valid class variable declaration was encountered
    private void classVarDeclar() {

        String dtype = null;
        String classVarKind = null;

        Token token = lexer.GetNextToken();
        if (isBeginClassVarDeclar(token)) {

            Ok(token);
            // static or field
            classVarKind = token.getLexeme();

            dtype = type();
            // determines if the type is declared in the current,
            // source file, otherwise it is added to the unresolved list.
            determineResolved(dtype);

            token = lexer.GetNextToken();
            if (isIdent(token)) {
                Ok(token);
                checkSymbolIsUndeclared(token, globalTable);
                Symbol s = new Symbol();
                s.kind = classVarKind;
                s.name = token.getLexeme();
                s.type = dtype;
                globalTable.insert(s);

                token = lexer.PeekNextToken();
                // loop until no more identifiers are declared
                while (is(token,",")) {
                    Ok(token);
                    // consume the ,
                    lexer.GetNextToken();

                    token = lexer.GetNextToken();
                    // a , has to be followed by an identifier
                    if (isIdent(token)) {
                        Ok(token);
                        checkSymbolIsUndeclared(token, globalTable);
                        Symbol sNextVar = new Symbol();
                        sNextVar.kind = classVarKind;
                        sNextVar.name = token.getLexeme();
                        sNextVar.type = dtype;
                        globalTable.insert(sNextVar);

                        token = lexer.PeekNextToken();
                    }
                    else {
                        Error(token, "Expected identifier got " + token.getType());
                    }
                }

                // the declaration ends with ;
                token = lexer.GetNextToken();
                if (is(token,";")) {
                    Ok(token);
                }
                else {
                    Error(token, "Expected ; got " + token.getLexeme());
                }
            }
            else {
                Error(token, "Expected identifier got " + token.getType());
            }
        }
        else {
            Error(token, "Expected static or field got " + token.getLexeme());
        }
    }

    // Determine if a valid subroutine declaration was encountered.
    private void subroutineDeclar() {

        // create a new table for this current subroutine
        methodTable = new SymbolTable();
        // populate the temp method table with types
        populateTypes(methodTable);
        funcDeclarName = null;
        funcDeclarReturn = null;
        currMethodName = null;
        methodRetType = null;
        String methodKind = null;

         Token token = lexer.GetNextToken();
         if (isBeginSubroutineDeclar(token)) {

             Ok(token);
             methodKind = token.getLexeme();

             token = lexer.PeekNextToken();

             // if it is not a valid type throw an error
             if ( !(isBeginType(token)
                     || is(token, "void"))) {

                 Error(token, "Expected type declaration got " + token.getLexeme());
             }

             // it is a beginning of a type declaration
             else if (isBeginType(token)) {
                 funcDeclarReturn = type();
                 // check if specified return type is already declared
                 determineResolved(funcDeclarReturn);
             }

             // it is of void type
             else if (is(token,"void")) {

                 Ok(token);
                 funcDeclarReturn = "void";
                 // consume the void
                 lexer.GetNextToken();
             }

             methodRetType = funcDeclarReturn;

             token = lexer.GetNextToken();
             // identifier expressing subroutine's name
             if (isIdent(token)) {
                 Ok(token);
                 checkSymbolIsUndeclared(token, globalTable);
                 funcDeclarName = token.getLexeme();
             }

             else {
                 Error(token, "Expected identifier got " + token.getType());
             }

             currMethodName = funcDeclarName;

             // Below the validity of parameter section of a method is checked
             token = lexer.GetNextToken();
             if (is(token,"(")) {
                 Ok(token);

                 // the list of parameters, possibly empty
                 // arguments to the subroutine are added within
                 // the first argument is always the this pointing to the class
                 Symbol thisArg = new Symbol();
                 thisArg.kind = "arg";
                 thisArg.type = classes.get(atClass);
                 thisArg.name = "this";
                 methodTable.insert(thisArg);
                 paramList();

                 token = lexer.GetNextToken();
                 if (is(token,")")) {
                     Ok(token);

                 }
                 else {
                     Error(token, "Expected ) got " + token.getLexeme());
                 }

                 codeCommands.add("function "
                     + classes.get(atClass)
                     + "."
                     + funcDeclarName
                     + " "
                     + methodTable.getStaticOrArgCount());

                 // 0 or more statements
                 subroutineBody();
                 mergeMethodTable(methodTable, funcDeclarName, funcDeclarReturn, methodKind);
                 if (notAllPaths) {
                     Warning(token, "WARNING! Not all code paths in "
                             + funcDeclarName + " return a value.");
                 }
                 if (!methodRetType.equals(funcDeclarReturn)) {
                     System.out.println("WARNING! Expected return type "
                             + funcDeclarReturn
                             + " got "
                             + methodRetType
                             + " in "
                             + funcDeclarName
                             + descriptiveMessage(token));
                 }
                 codeCommands.add("return");
                 // the current method routine is finished, cleans up the table.
                 methodTable = new SymbolTable();
                 // the current method has returned, cleans flag
             }
             else {
                 Error(token, "Expected ( got " + token.getLexeme());
             }

         }
         else {
             Error(token, "Expected constructor, function or method got " + token.getLexeme());
         }
    }

    // Determines if a valid parameter list is supplied.
    private void paramList() {

        String dtype = null;

        Token token = lexer.PeekNextToken();
        // each parameter has a type and an appropriate identifier
        if (isBeginType((token))) {

            dtype = type();
            determineResolved(dtype);

            token = lexer.GetNextToken();
            if (isIdent(token)) {
                Ok(token);
                checkSymbolIsUndeclared(token, methodTable);
                Symbol sArg = new Symbol();
                sArg.kind = "arg";
                sArg.type = dtype;
                sArg.name = token.getLexeme();
                methodTable.insert(sArg);

                token = lexer.PeekNextToken();
                // when more parameters are encountered
                while (is(token,",")) {
                    Ok(token);
                    // consume the ,
                    lexer.GetNextToken();

                    dtype = type();

                    token = lexer.GetNextToken();
                    if (isIdent(token)) {
                        Ok(token);
                        checkSymbolIsUndeclared(token, methodTable);
                        Symbol sArgNext = new Symbol();
                        sArgNext.kind = "arg";
                        sArgNext.type = dtype;
                        sArgNext.name = token.getLexeme();
                        methodTable.insert(sArgNext);
                    }
                    else {
                        Error(token, "Expected identifier got " + token.getType());
                    }

                    token = lexer.PeekNextToken();
                }

            }
            else {
                Error(token, "Expected identifier got " + token.getType());
            }
        }

    }

    /*
    * Determines if 0 or more valid statements are provided.
    * Nesting is allowed, as well as an empty body.*/
    private void subroutineBody() {

        Token token = lexer.GetNextToken();
        // opening brace found
        if (is(token,"{")) {

            Ok(token);

            token = lexer.PeekNextToken();
            // when more statements are encountered
            while (isBeginStatement(token)) {
                /*
                 * anything between the braces are the statements,
                 * check if they are valid. */
                if (returnEncountered) {
                    Warning(token, "Unreachable code detected in "
                            + currMethodName);
                }
                statement();
                token = lexer.PeekNextToken();
            }

            returnEncountered = false;

            // check for a closing brace
            token = lexer.GetNextToken();
            if (is(token,"}")) {
                Ok(token);
            }
            else {
                Error(token, "Expected } got " + token.getLexeme());
            }
        }
        else {
            Error(token, "Expected { got " + token.getLexeme());
        }

    }

    // Checks if a valid statement is encountered.
    private void statement() {
        Token token = lexer.PeekNextToken();

        if(is(token,"var")) {
            varDeclarStatement();
        }

        else if(is(token,"let")) {
            letStatement();
        }

        else if(is(token,"if")) {
            ifStatement();
        }

        else if(is(token,"while")) {
            whileStatement();
        }

        else if(is(token,"do")) {
            doStatement();
        }

        else if(is(token,"return")) {
            returnStatement();
        }

        else {
            Error(token, "Expected statement keyword got " + token.getLexeme());
        }
    }

    // Checks whether a valid if statement was encountered
    private void ifStatement() {

        Token token = lexer.GetNextToken();

        // every if statement begins with an if
        if (is(token,"if")) {
            Ok(token);
            codeCommands.add("label ifcond");
            inIf = true;

            // check the enclosed expression
            token = lexer.GetNextToken();
            if (is(token, "(")) {
                Ok(token);

                expression();

                token = lexer.GetNextToken();
                if (is(token, ")")) {
                    Ok(token);
                    codeCommands.add("neg");
                    codeCommands.add("if-goto elseif");

                    // followed by 0 or more statements
                    subroutineBody();

                    // possibly followed by an else statement
                    token = lexer.PeekNextToken();
                    if (is(token, "else")) {
                        Ok(token);
                        inElse = true;
                        // consume the else
                        lexer.GetNextToken();

                        // statements within the else
                        subroutineBody();
                    }
                    codeCommands.add("label elseif");

                    // finally clear flags
                    inIf = false;
                    inElse = false;

                } else {
                    Error(token, "Expected ) got " + token.getLexeme());
                }
            } else {
                Error(token, "Expected ( got " + token.getLexeme());
            }
        } else {
            Error(token, "Expected if got " + token.getLexeme());
        }

    }

    // Determines whether a valid let (assignment) statement is found
    private void letStatement() {
        Symbol s = null;
        // every such statement begins with the keyword let
        Token token = lexer.GetNextToken();
        if (is(token, "let")) {
            Ok(token);

            // followed by the identifier to be assigned
            token = lexer.GetNextToken();
            if (isIdent(token)) {
                Ok(token);
                // checks if identifier resides within the method
                s = methodTable.lookup(token.getLexeme());
                // if not, then also checks the global table
                if (s == null) {
                    s = globalTable.lookup(token.getLexeme());
                }
                // if it is not found both method and global table
                if (s == null) {
                    Warning(token, "Variable with name " + token.getLexeme()
                            + " is not declared.");
                }
                if (! (s.kind.equals("var")
                        || s.kind.equals("static")
                        || s.kind.equals("field"))) {
                    Warning(token, "Identifier was not declared as variable.");
                }
            }
            else {
                Error(token, "Expected identifier got " + token.getType());
            }

            token = lexer.PeekNextToken();
            // check if its an array declaration
            if (is(token,"[")) {
                Ok(token);
                if(!s.type.equals("Array")) {
                    Warning(token, "Variable was not declared as an array.");
                }
                // consume the [
                lexer.GetNextToken();

                expression();

                token = lexer.GetNextToken();
                if (is(token,"]")) {
                    Ok(token);
                }
                else {
                    Error(token, "Expected ] got " + token.getLexeme());
                }
            }

            // assignment operator
            token = lexer.GetNextToken();
            if (is(token,"=")) {
                Ok(token);

                expression();
                if (!s.type.equals(currExprType)
                        && !isUnresolved(currExprType)) {
                    Warning(token, "Expected assignment of "
                            + s.type
                            + " got "
                            + currExprType);
                }
                s.valueSet = true;
                // value is set by popping what is on top of the staff
                // to the address of the identifier
                codeCommands.add("pop "
                        + determineVarCommandKind(s));

                // end of statement terminal ;
                token = lexer.GetNextToken();
                if (is(token,";")) {
                    Ok(token);
                }
                else {
                    Error(token, "Expected ; got " + token.getLexeme());
                }
            }
            else {
                Error(token, "Expected = got " + token.getLexeme());
            }
        }
        else {
            Error(token, "Expected 'let' got " + token.getLexeme());
        }
    }

    // Determines if a valid variable declaration statement is encountered
    private void varDeclarStatement() {

        String dtype = null;
        Token token = lexer.GetNextToken();

        // begins with the keyword var
        if (is(token,"var")) {
            Ok(token);

            // the type of the variable to be declared
            dtype = type();

            // followed by an identifier naming the variable
            token = lexer.GetNextToken();
            if (isIdent(token)) {
                Ok(token);
                // declaration in method, local scope needs to be considered
                if (methodTable.getAllSymbols().size() != 0) {
                    checkSymbolIsUndeclared(token, methodTable);
                }

                Symbol s = new Symbol();
                s.name = token.getLexeme();
                s.type = dtype;
                s.kind = "var";

                methodTable.insert(s);

                // possibly more than one variable declarations in the same statement
                token = lexer.PeekNextToken();
                while (is(token,",")) {
                    Ok(token);
                    // consume the ,
                    lexer.GetNextToken();

                    // the , must always be followed by an identifier for continuation
                    token = lexer.PeekNextToken();
                    if (isIdent(token)) {
                        Ok(token);
                        // declaration in method, local scope needs to be considered
                        if (methodTable.getAllSymbols().size() != 0 ) {
                            checkSymbolIsUndeclared(token, methodTable);
                        }
                        // otherwise it is the global table that needs to be checked
                        else if (methodTable.getAllSymbols().size() == 0) {
                            checkSymbolIsUndeclared(token, globalTable);
                        }

                        Symbol sNew = new Symbol();
                        sNew.name = token.getLexeme();
                        sNew.type = dtype;
                        sNew.kind = "var";

                        methodTable.insert(sNew);

                        // consume the identifier
                        lexer.GetNextToken();

                        // to possibly continue iterating identifiers, without consuming
                        token = lexer.PeekNextToken();
                    }
                    else {
                        Error(token,"Expected identifier got " + token.getType());
                    }
                }

                // finally termianted by a ;
                if(token.getLexeme().equals(";")) {
                    Ok(token);
                    // consume the ;
                    lexer.GetNextToken();
                }
                else {
                    Error(token, "Expected ; got " + token.getLexeme());
                }
            }
            else {
                Error(token, "Expected identifier got " + token.getType());
            }
        }
        else {
            Error(token, "Expected 'var' got " + token.getLexeme());
        }
    }

    /*
    * Checks if the next token annotates a valid type.
    * This can be both pre-determined jack programming language types
    * OR a type identifier previously encountered in the source file. */
    private String type() {

        String dtype = null;
        Token token = lexer.GetNextToken();

        if (is(token,"int")) {
            Ok(token);
            dtype = "int";
        }
        else if (is(token,"char")) {
            Ok(token);
            dtype = "char";
        }
        else if (is(token,"boolean")) {
            Ok(token);
            dtype =  "boolean";
        }
        else if (isIdent(token)) {
            Ok(token);
            dtype =  token.getLexeme();
        }

        else {
            Error(token, "Expected a type declaration got " + token.getLexeme());
        }

        return dtype;
    }

    // Determines if a valid while loop statement is encountered.
    private void whileStatement() {

        // every while statement beigns with the keyword while
        Token token = lexer.GetNextToken();
        if (is(token,"while")) {
            Ok(token);
            codeCommands.add("label loop");

            /*
            * followed by the paranthesised expression which determines
            * until which point the while loop executes. */
            token = lexer.GetNextToken();
            if (is(token, "(")) {
                Ok(token);

                expression();

                token = lexer.GetNextToken();
                if (is(token, ")")) {
                    Ok(token);
                }
                else {
                    Error(token, "Expected ')' got " + token.getLexeme());
                }

                // if condition does not hold, go to the end of loop
                codeCommands.add("neg");
                codeCommands.add("if-goto end");
                subroutineBody();
                codeCommands.add("label end");
            }

            else {
                Error(token, "Expected ( got " + token.getLexeme());
            }
        }
    }

    // Determines if a valid do statement is encountered
    private void doStatement() {

        // it should begin with the keyword do
        Token token = lexer.GetNextToken();
        if (is(token, "do")) {
            Ok(token);

            // determines if a valid subroutine is provided for execution
            subroutineCall();

            // end of statement indicated by a ;
            token = lexer.GetNextToken();
            if (is(token, ";")) {
                Ok(token);
            }

            else {
                Error(token, "Expected ; got " + token.getLexeme());
            }
        }
        else {
            Error(token, "Expected do got " + token.getLexeme());
        }
    }

    // Determines if a section of code follows the grammar of defining a subroutine
    private void subroutineCall() {

        boolean isResolved = false;
        String objectName = null;
        String objectRoutine = null;

        Token token = lexer.GetNextToken();
        // the identifier of the routine
        if (isIdent(token)) {
            SymbolTable t = globalTable;
            Ok(token);
            objectName = token.getLexeme();
            Symbol s = globalTable.lookup(objectName);
            if (s == null) {
                // it might be the routine of another class
                for (String className: classes) {
                    if (className.equals(objectName)) {
                        isResolved = true;
                    }
                }
            }

            token = lexer.PeekNextToken();
            // possibly the routine of a routine
            if (is(token, ".")) {
                Ok(token);
                // consume the .
                lexer.GetNextToken();

                token = lexer.GetNextToken();
                if (isIdent(token)) {
                    Ok(token);
                    objectRoutine = token.getLexeme();
                }
                else {
                    Error(token, "Expected identifier got " + token.getType());
                }
            }

            // marks the routine as unresolved,if declaration was not found
            // in currently parsed class symbol tables
            if (!isResolved) {
                unresolved.put(objectName, objectRoutine);
                unresolvedMessage.add(descriptiveMessage(token)
                        + " In source file " + classes.get(atClass));
            }
            // otherwise a declaration was found and checks have to be performed
            // if the symbol is indeed a method of that class.
            else {
                for (int i=0; i < classes.size(); i++) {
                    if (classes.get(i).equals(objectName)) {
                        t = classTables.get(i);
                        Symbol routine = t.lookup(objectRoutine);
                        if (routine == null) {
                            System.out.println("WARNING! Routine with name "
                                    + objectRoutine + " is not declared in class "
                                    + classes.get(i));
                        } else {
                            if (!(routine.kind.equals("constructor") || routine.kind.equals("function")
                                    || routine.kind.equals("method"))) {
                              System.out.println("WARNING! " + objectRoutine + " is not declared as routine in class "
                                      + objectName);
                            }
                        }
                    }
                }
            }

            token = lexer.GetNextToken();
            // determines if valid expressions are supplied as arguments to the routine
            if (is(token, "(")) {
                Ok(token);

                String routine = objectName;
                boolean isObjectRoutine = false;
                if (objectRoutine != null) {
                    routine = routine + "." + objectRoutine;
                    isObjectRoutine = true;
                }

                String commandToAdd = routine;

                // if the method of another object will be accessed
                if (isObjectRoutine) {
                    Symbol theObj = globalTable.lookup(objectName);
                    if (theObj == null) {
                        theObj = methodTable.lookup(objectName);
                    }
                    if (theObj == null) {
                        if (!isDefaultVMClass(objectName)) {
                            codeCommands.add("push argument 0");
                        }
                    }
                    if (theObj != null) {
                        String theType = determineVarCommandKind(theObj);
                        commandToAdd = theObj.type + "." + objectRoutine;
                        codeCommands.add("push " + theType);
                    }
                }

                expressionList();
                if (!isResolved) {
                    unresolvedArgs.put(routine, callArgs);
                    unresolvedArgsMessage.add(descriptiveMessage(token)
                            + " In source file " + classes.get(atClass));
                } else {
                    checkArgsMatch(routine, callArgs, t, descriptiveMessage(token));
                }

                // clears the args
                int commandArgSize = callArgs.size() + 1;
                if (isDefaultVMClass(objectName)) {
                    commandArgSize = callArgs.size();
                }

                codeCommands.add("call " + commandToAdd + " " + commandArgSize);
                callArgs = new ArrayList<>();

                currMethodName = routine;

                token = lexer.GetNextToken();
                if (is(token, ")")) {
                    Ok(token);
                }
                else {
                    Error(token, "Expected ) got " + token.getLexeme());
                }
            }

            else {
                Error(token, "Expected ( got " + token.getLexeme());
            }
        }

        else {
            Error(token, "Expected an identifier got " + token.getType());
        }
    }

    // Determines if a valid return statement is encountered.
    private void returnStatement() {

        notAllPaths = false;

        // each return statement begins with the keyword return
        Token token = lexer.GetNextToken();
        if (is(token, "return")) {
            Ok(token);

            // flag to check if the return is a void, i.e. eps
            boolean isVoid = true;
            // followed by some expression
            token = lexer.PeekNextToken();
            if (isBeginExpression(token)) {
                expression();
                methodRetType = currExprType;
                // an expression occurred, hence not a void return
                isVoid = false;
            }

            if (isVoid) {
                methodRetType = "void";
                codeCommands.add("push constant 0");
            }

            if (inIf) {
                if (!inElse) {
                    notAllPaths = true;
                }
            }
            if (!inIf) {
                notAllPaths = false;
                returnEncountered = true;
            }

            token = lexer.PeekNextToken();
            if (is(token, ";")) {
                Ok(token);
                // consume ;
                lexer.GetNextToken();
            }
            else {
                Error(token, "Expected ; got " + token.getLexeme());
            }

        }

        else {
            Error(token, "Expected 'return' got" + token.getLexeme());
        }
    }

    // Determines if a valid list of expressions is encountered
    private void expressionList() {

        Token token = lexer.PeekNextToken();
        /*
        * Peek if the next token is the beginning of an expression,
        * since the production rule also accepts an empty input. */
        if (isBeginExpression(token)) {

            expression();
            this.callArgs.add(currExprType);

            // possibly many expressions separated by ,
            token = lexer.PeekNextToken();
            // loop until there are no more expressions
            while (is(token, ",")) {
                Ok(token);
                // consume the operator symbol
                lexer.GetNextToken();

                // , must be followed by a valid expression
                expression();
                this.callArgs.add(currExprType);

                // the loop terminates when the next token is not ,
                token = lexer.PeekNextToken();
            }
        }
    }

    // Determine if a valid expression is encountered
    private void expression() {
        relationalExpression();

        // peek for possible superceeding boolean operators
        Token token = lexer.PeekNextToken();
        while (is(token, "&")
                || is(token, "|")) {
            Ok(token);
            currExprType = "boolean";
            // to check what type of operation to perform
            boolean and = false;
            if (token.getLexeme().equals("&")) {
                and = true;
            }
            // consume the operator symbol
            lexer.GetNextToken();
            relationalExpression();
            if (and) {
                codeCommands.add("and");
            } else {
                codeCommands.add("or");
            }
            token = lexer.PeekNextToken();
        }
    }

    // Determines if an expression is a valid relational expression
    private void relationalExpression() {

        arithmeticExpression();

        // possibly followed by arithmetic comparison operators
        Token token = lexer.PeekNextToken();
        while (is(token, "=")
                || is(token, ">")
                || is(token, "<")) {
            Ok(token);
            currExprType = "boolean";
            boolean lt = false;
            boolean eq = false;
            // checks what type of arithmetic should be performed
            if (token.getLexeme().equals("<")) {
                lt = true;
            }
            if (token.getLexeme().equals("=")) {
                eq = true;
            }
            // consume the operator symbol
            lexer.GetNextToken();
            // followed by another arithmetic expression
            arithmeticExpression();

            if (lt) {
                codeCommands.add("lt");
            } else if (eq) {
                codeCommands.add("eq");
            } else {
                codeCommands.add("gt");
            }

            // end the while loop when no operator follows an expression
            token = lexer.PeekNextToken();
        }
    }

    // Determines if a valid arithmetic expression is encountered
    private void arithmeticExpression() {

        term();

        // possibly multiple terms added or subtracted together
        Token token = lexer.PeekNextToken();
        // iterate until no more additions or subtractions of terms are found
        while (is(token,"+")
                || is(token, "-")) {
            Ok(token);
            currExprType = "int";
            boolean add = false;
            if (token.getLexeme().equals("+")) {
                add = true;
            }
            // consume the operator symbol
            lexer.GetNextToken();

            term();
            if (add) {
                codeCommands.add("add");
            } else {
                codeCommands.add("sub");
            }

            token = lexer.PeekNextToken();
        }
    }

    // Determines if a valid term is encountered
    private void term() {

        factor();

        // possibly multiple factors multiplied or divided amongst each other
        Token token = lexer.PeekNextToken();
        while (is(token, "*")
                || is(token, "/")) {
            Ok(token);
            currExprType = "int";
            boolean mult = false;
            if (token.getLexeme().equals("*")) {
                mult = true;
            }
            // consume the operator symbol
            lexer.GetNextToken();

            factor();
            if (mult) {
                //codeCommands.add("push argument 0");
                codeCommands.add("call Math.multiply 2");
            } else {
                //codeCommands.add("push argument 0");
                codeCommands.add("call Math.div 2");
            }
            token = lexer.PeekNextToken();
        }
    }

    // Determines if a valid factor is encountered
    private void factor() {

        //a factor could be preceeded by a negation or ~ symbol

        Token token = lexer.PeekNextToken();
        if (is(token, "-")) {
            Ok(token);
            currExprType = "int";
            codeCommands.add("neg");
            // consume the -
            lexer.GetNextToken();
        }

        else if (is(token,"~")) {
            Ok(token);
            codeCommands.add("not");
            // consume the ~
            lexer.GetNextToken();
        }

        operand();
    }

    // Determines if a valid operand is encountered
    private void operand(){
        // the operand could be a number
        Token token = lexer.PeekNextToken();
        if (token.getType() == TokenType.num) {

            Ok(token);
            currExprType = "int";
            codeCommands.add("push constant "
                    + token.getLexeme());
            // consume the number
            lexer.GetNextToken();
            return;
        }

        // or an identifier
        else if (isIdent(token)) {

            Ok(token);
            boolean isResolved = true;
            // to indicate if the value is set by the param list,
            // instead of global table
            boolean methodArg = false;

            String objName = token.getLexeme();
            String objLocality = objName;
            String kindOfLocality = objLocality;
            Symbol s = methodTable.lookup(objName);
            if (s != null) {
                 if (s.kind.equals("arg")) {
                     methodArg = true;
                 }
            }
            if (s == null) {
                s = globalTable.lookup(objName);
            }
            if (s == null) {
                unresolved.put(objName, null);
                unresolvedMessage.add(descriptiveMessage(token)
                        + " In source file " + classes.get(atClass));
                kindOfLocality = objName;
                isResolved = false;
            }
            if (s != null) {
                objLocality = determineVarCommandKind(s);
            }
            String objSubName = null;
            // consume the identifier
            lexer.GetNextToken();

            // possibly identifier's method or variable
            token = lexer.PeekNextToken();
            if (is(token, ".")) {
                Ok(token);
                lexer.GetNextToken();

                token = lexer.GetNextToken();
                if (isIdent(token)) {
                    Ok(token);
                    objSubName = token.getLexeme();
                    if (!isResolved) {
                        unresolved.put(objName, objSubName);
                        unresolvedMessage.add(descriptiveMessage(token)
                                + " In source file " + classes.get(atClass));
                    }
                    kindOfLocality = kindOfLocality + "." + objSubName;
                }
                else {
                    Error(token, "Expected identifier got " + token.getType());
                }
            }

            Symbol newS = null;
            // local address of the object can be found
            if (isResolved && !kindOfLocality.equals(objName)){
                newS = globalTable.lookup(objSubName);
                if (newS == null) {
                    for (int i=0; i < classes.size(); i++) {
                        if (classes.get(i).equals(objName)) {
                            newS = classTables.get(i).lookup(objSubName);
                            if (newS == null) {
                                Warning(token, objSubName
                                        + " is not defined in class "
                                        + classes.get(i));
                            }
                            break;
                        }
                    }
                }
                kindOfLocality = determineVarCommandKind(newS);
            }

            String routine = objName;
            if (objSubName != null) {
                routine = routine + "." + objSubName;
            }

            // possibly an array
            token = lexer.PeekNextToken();
            if (is(token,"[")) {
                Ok(token);
                // if it is not an object and is resolved, then
                // it must be an array
                if (isResolved && objSubName == null) {
                    if (!s.type.equals("Array")) {
                        Warning(token, objName
                                + " was not declared as an array.");
                    }
                }
                // consume the [
                lexer.GetNextToken();
                // the vm instructions referencing the array
                if (isResolved) {
                    codeCommands.add("push "
                            + objLocality);
                    // in order to reference the object
                    codeCommands.add("pop pointer 1");
                    if (objSubName != null) {
                        // to reference the object's inner var array
                        codeCommands.add("push "
                                + kindOfLocality);
                    }
                } else {
                    codeCommands.add("push " + routine);
                    // in order to reference the object
                    codeCommands.add("pop pointer 1");
                }

                // preserve prev exp eval type since arrays and class
                // attributes can can be of any type
                String prevType = currExprType;
                // an expression, empty or for instance evaluating the size of the array
                expression();
                if (currExprType == null) {
                    Warning(token, "Expressions in arrays must"
                            + " evaluate to int value. Got none");
                }
                // to jump to the index of the array object
                boolean typeKnown = true;
                for (Map.Entry<String, String> entry : unresolved.entrySet()) {
                    if (entry.getKey().equals(currExprType)) {
                        typeKnown = false;
                    }
                }
                // if the type of expression evaluation is not unresolved
                if (typeKnown) {
                    if (!currExprType.equals("int")) {
                        Warning(token, "Expressions in arrays must"
                                + " evaluate to int value. Got "
                                + currExprType);
                    }
                }
                codeCommands.add("add");
                if (currExprType != null) {
                    currExprType = prevType;
                } else {
                    currExprType = "Array";
                }

                // next token after the expression should be ]
                token = lexer.GetNextToken();
                if (is(token, "]")) {
                    Ok(token);
                    return;
                } else {
                    Error(token, "Expected ']' got " + token.getLexeme());
                }
            }

            // possibly a method
            if (is(token,"(")) {
                Ok(token);
                // consume the (
                lexer.GetNextToken();

                // pushes the identifiers
                if (isResolved) {
                    currExprType = s.type;
                    if (newS != null) {
                        currExprType = newS.type;
                    } else {
                        if (objSubName != null) {
                            Warning(token, objSubName
                                    + " is not a declared method of class "
                                    + objName);
                        }
                    }
                    codeCommands.add("push "
                            + objLocality);
                }
                // not resolved
                else {
                    currExprType = routine;
                    //codeCommands.add("push " + routine);
                }
                // adjust the that pointer
                //codeCommands.add("pop pointer 1");

                // in order to restore the call args to previous state
                int currIndex = callArgs.size()+1;
                String oldExprType = currExprType;
                expressionList();
                ArrayList<String> thisArgs = cutList(callArgs, currIndex);
                if (!isResolved) {
                    unresolvedArgs.put(routine, thisArgs);
                    unresolvedArgsMessage.add(descriptiveMessage(token)
                            + " In source file " + classes.get(atClass));
                } else {
                    checkArgsMatch(routine, thisArgs, globalTable, descriptiveMessage(token));
                }
                for (int i = currIndex; i < callArgs.size(); i++) {
                    callArgs.remove(i);
                }

                currExprType = oldExprType;
                codeCommands.add("call " + routine + " " + String.valueOf(thisArgs.size()));

                if (isResolved) {
                    // if there is no object's subroutine defined and resolved
                    if (newS == null) {
                        // the expression type is the return type of the method
                        currExprType = s.type;
                    } else { // the type is the return of the subroutine
                        currExprType = newS.type;
                    }
                }

                // next token after the expressionList should be )
                token = lexer.GetNextToken();
                if (is(token,")")) {
                    Ok(token);
                    return;
                } else {
                    Error(token, "Expected ')' got " + token.getLexeme());
                }
            }

            // pushes the identifiers
            if (isResolved) {
                // if it is not an argument
                if (!methodArg && !s.valueSet) {
                    Warning(token, "All values used in expressions"
                            + " must be set to a value first.");
                }
                if (objSubName == null) {
                    currExprType = s.type;
                    codeCommands.add("push "
                            + objLocality);
                } else {
                    currExprType = newS.type;
                    codeCommands.add("push "
                            + kindOfLocality);
                }
            }
            // not resolved
            else {
                currExprType = objName + "." + objSubName;
                codeCommands.add("push " + currExprType);
            }
        }

        // or it could be a beginning of a paranthesised expression
        else if (is(token,"(")){
            Ok(token);
            // consume the (
            lexer.GetNextToken();

            expression();

            token = lexer.GetNextToken();
            if (is(token,")")) {
                Ok(token);
                return;
            }

            else {
                Error(token, "Expected ')' got " + token.getLexeme());
            }
        }

        // lastly, it could be one of the following keywords or a string literal
        else if (token.getLexeme().equals("true")
                || token.getLexeme().equals("false")
                || token.getLexeme().equals("null")
                || token.getLexeme().equals("this")
                || token.getType() == TokenType.stringLiteral) {
            Ok(token);
            currExprType = "boolean";
            if (token.getLexeme().equals("true")) {
                codeCommands.add("push constant 1");
            }
            if (token.getLexeme().equals("false")) {
                codeCommands.add("push constant 0");
            }
            if (token.getLexeme().equals("null")) {
                if (methodRetType == null) {
                    currExprType = classes.get(atClass);
                } else {
                    currExprType = methodRetType;
                }
                codeCommands.add("push constant 0");
            }
            if (token.getLexeme().equals("this")) {
                currExprType = classes.get(atClass);
                codeCommands.add("push argument 0");
            }
            if (token.getType() == TokenType.stringLiteral) {
                currExprType = "String";
                codeCommands.add("push constant " + token.getLexeme());
            }
            lexer.GetNextToken();
        }

        else {
            Error(token, "Expected an operand got " + token.getLexeme());
        }
    }

    // The below methods check if the token matches the beginning of given productions

    // a class variable declaration begins with either a static or field
    private boolean isBeginClassVarDeclar(Token token){

        if (token.getLexeme().equals("static")
                || token.getLexeme().equals("field")) {

            return true;
        }

        return false;
    }

    // a subroutine begins with the keyword constructor, fucntion or method
    private boolean isBeginSubroutineDeclar(Token token){

        if (token.getLexeme().equals("constructor")
                || token.getLexeme().equals("function")
                || token.getLexeme().equals("method")) {

            return true;
        }

        return false;
    }

    // a type is either a predefined Jack language type or a source-found identifier
    private boolean isBeginType(Token token) {

        if ( token.getLexeme().equals("int")
                || token.getLexeme().equals("char")
                || token.getLexeme().equals("boolean")
                || token.getType() == TokenType.id) {

            return true;
        }

        return false;
    }

    // each statement begins with either of the below specified keywords
    private boolean isBeginStatement(Token token) {

        if (token.getLexeme().equals("var")
                || token.getLexeme().equals("let")
                || token.getLexeme().equals("if")
                || token.getLexeme().equals("while")
                || token.getLexeme().equals("do")
                || token.getLexeme().equals("return")) {

            return true;
        }

        return false;
    }

    // a beginning of an expression is either a beginning of a factor or an operand
    private boolean isBeginExpression(Token token) {

        if (isBeginFactor(token) || isBeginOperand(token)){
            return true;
        }

        return false;
    }

    // a factor begins with any of the unary operations
    private boolean isBeginFactor(Token token) {

        if (token.getLexeme().equals("-")
                || token.getLexeme().equals("~")) {
            return true;
        }

        return false;
    }

    // an operand begins with either a number, identifier or a string literal
    private boolean isBeginOperand(Token token) {

        if (token.getType() == TokenType.num
                || token.getType() == TokenType.id
                || token.getType() == TokenType.stringLiteral
                || token.getLexeme().equals("(")
                || token.getLexeme().equals("true")
                || token.getLexeme().equals("false")
                || token.getLexeme().equals("null")
                || token.getLexeme().equals("this")) {
            return true;
        }

        return false;
    }

    // a utility method to check if a token is an identifier
    private boolean isIdent(Token token) {
        if (token.getType() == TokenType.id) {
            return true;
        }

        return false;
    }

    // a utility method to check if a token's lexeme matches a given string
    private boolean is(Token token, String str) {
        if (token.getLexeme().equals(str)) {
            return true;
        }
        else {
            return false;
        }
    }

    // a utlity method to check if the name is resolved in the
    // current global symbol table.
    public void determineResolved(String name) {
        Symbol s = globalTable.lookup(name);
        if (s == null) {
            unresolved.put(name, null);
        }
    }

    // All type annotations and methods which could not be resolved
    // during parsing, should be resolved after all source files'
    // symbol tables have been successfully extracted.
    public void resolve() {
        String className = null;

        int atUnresolved = 0;
        int atUnresolvedArgs = 0;
        for (Map.Entry<String, String> entry : unresolved.entrySet()) {
            boolean classFound = false;
            // the possible reference to a class
            className = entry.getKey();
            for (int i=0; i < classes.size(); i ++) {
                if (className.equals(classes.get(i))) {
                    classFound = true;
                    String objRoutine = entry.getValue();
                    // a possible routine or var of that class
                    if (objRoutine != null) {
                        String fullName = className + "." + objRoutine;
                        ArrayList<String> theArgs = null;
                        boolean isMethod = false;
                        if (unresolvedArgs != null) {
                            atUnresolvedArgs = 0;
                            for (Map.Entry<String, ArrayList<String>> arg :
                                    unresolvedArgs.entrySet()) {
                                if (arg.getKey().equals(fullName)) {
                                    isMethod = true;
                                    theArgs = arg.getValue();
                                    break;
                                }
                                atUnresolvedArgs++;
                            }
                        }
                        if (isMethod) {
                            Symbol s = classTables.get(i).lookup(objRoutine);
                            // routine is defined in the classes' symbol table
                            if (s == null) {
                                System.out.println("WARNING! Undeclared routine"
                                        + objRoutine
                                        + " of class "
                                        + classes.get(i) + " when resolving external method calls."
                                        + unresolvedArgsMessage.get(atUnresolvedArgs));

                            }
                            else {
                                if (! (s.kind.equals("constructor")
                                        || s.kind.equals("function")
                                        || s.kind.equals("method"))) {
                                    System.out.println("WARNING! When calling "
                                            + objRoutine
                                            + " of class "
                                            + classes.get(i) + ". It was declared as "
                                            + s.kind
                                            + unresolvedArgsMessage.get(atUnresolvedArgs));
                                }
                            }

                            // finally checks if the arguments of the method call match
                            checkArgsMatch(fullName, theArgs, classTables.get(i), unresolvedArgsMessage.get(atUnresolvedArgs));
                        } else {
                            Symbol classVar = classTables.get(i).lookup(objRoutine);
                            if (classVar == null) {
                                System.out.println("WARNING! When resolving "
                                        + fullName
                                        + ". No field or static class variable of name "
                                        + objRoutine
                                        + " is declared in class "
                                        + className
                                        + unresolvedMessage.get(atUnresolved));
                            } else if (!(classVar.kind.equals("field")
                                    || classVar.kind.equals("static"))) {
                                System.out.println("WARNING! When resolving "
                                        + fullName
                                        + ".Identifier "
                                        + objRoutine
                                        + " is not declared as field or static in class "
                                        + className
                                        + unresolvedMessage.get(atUnresolved));
                            }
                        }
                    }
                }
            }
            if (!classFound) {
                System.out.println("WARNING! Object of type "
                        + className
                        + " could not be resolved. No class with that name"
                        + " is found in this directory."
                        + unresolvedMessage.get(atUnresolved));
            }
            atUnresolved++;
        }
    }

    // Given a routine name, the arguments supplied to the routine and
    // the corresponding symbol table, determines if a valid call to
    // that method has been performed, i.e. matching method and params.
    private void checkArgsMatch(String routine, ArrayList<String> args,
                                SymbolTable t, String errMessage) {
        // possibly a subroutine of a class object was called
        String[] routineSplit = routine.split("\\.");
        String className = routine;
        String method = null;
        // the method is the string after the .
        if (routineSplit.length == 2) {
            className = routineSplit[0];
            method = routineSplit[1];
        } else {
            method = routine;
        }
        // finds the method we are trying to call
        Symbol actualMethod;
        actualMethod = t.lookup(method);
        // was not found
        if (actualMethod == null) {
            System.out.println("WARNING! Method of name "
                    + method
                    + " is not defined in "
                    + className
                    + errMessage);
        }
        // the number of arguments must match
        // note that the this argument does not
        // need to be supplied on call.
        int actualArgCount = actualMethod.methodTable.getStaticOrArgCount();
        if (args.size() != actualArgCount-1) {
            System.out.println("WARNING! The number of arguments of routine call "
                    + method
                    + " does not match the number of arguments declared."
                    + errMessage);
        }
        int atArg = 0;
        for (Symbol s: actualMethod.methodTable.getAllSymbols()) {
            // the symb is an argument
            if (s.kind.equals("arg")) {
                // and it is not the this arg
                if (!s.name.equals("this")) {
                    // if there is a mismatch between types
                    if (!s.type.equals(args.get(atArg))) {
                        System.out.println("WARNING! Type of argument did not match"
                                + " expected type"
                                + " when calling method "
                                + method
                                + errMessage);
                    }
                }
            }
        }
    }

    // utlity method to get a subset of the list from a given index
    private ArrayList<String> cutList(ArrayList<String> list, int from) {
        ArrayList<String> newList = new ArrayList<>();
        for (int i=from; i < list.size(); i++) {
            newList.add(list.get(i));
        }
        return newList;
    }

    // determines if the given symbol name is currently unresolvable
    private boolean isUnresolved(String name) {
        for (Map.Entry<String,String> entry : unresolved.entrySet()) {
            if (entry.getKey().equals(name)) {
                return true;
            }
        }

        for (Map.Entry<String,ArrayList<String>> entry : unresolvedArgs.entrySet()) {
            if (entry.getKey().equals(name)) {
                return true;
            }
        }

        return false;
    }

    // formats labelling after parsing has been completed (for loops etc)
    private void formatCodeCommands() {
        int atWhile = 0;
        int atIf = 0;
        Stack<Integer> loopEndTrace = new Stack<Integer>();
        Stack<Integer> ifEndTrace = new Stack<Integer>();

        for (int i=0; i < codeCommands.size(); i++) {
            String finalCommand = codeCommands.get(i);
            String[] commandComponents = codeCommands.get(i).split(" ");
            if (commandComponents.length > 1) {

                if (commandComponents[0].equals("label")) {
                    if (commandComponents[1].equals("loop")) {
                        finalCommand = finalCommand + atWhile;
                        loopEndTrace.push(atWhile);
                        atWhile++;
                    }
                    if (commandComponents[1].equals("end") && !loopEndTrace.isEmpty()) {
                        finalCommand = finalCommand + loopEndTrace.pop();
                    }
                    if (commandComponents[1].equals("ifcond")) {
                        finalCommand = finalCommand + atIf;
                        ifEndTrace.push(atIf);
                        atIf++;
                    }
                    if (commandComponents[1].equals("elseif") && !ifEndTrace.isEmpty()) {
                        finalCommand = finalCommand + ifEndTrace.pop();
                    }
                }
                if (commandComponents[0].equals("if-goto")) {
                    if (commandComponents[1].equals("end")) {
                        finalCommand = finalCommand + (atWhile-1);
                    }
                    else {
                        finalCommand = finalCommand + (atIf-1);
                    }
                }
            }

            codeCommands.set(i, finalCommand);
        }
    }

    // checks if an object name belongs to one of the jack
    // predefined classes.
    private boolean isDefaultVMClass(String name) {
        String[] defaultClasses = {
            "Array",
            "String",
            "Keyboard",
            "Output",
            "Memory",
            "Sys",
            "Screen"};

        for (String str : defaultClasses) {
            if (name.equals(str)) {
                return true;
            }
        }

        return false;
    }

    // creates the according vm files of each jack source file
    private void createVMFiles() throws IOException{
        boolean classLine = false;
        boolean classEncountered = false;
        FileWriter vmFile = null;

        for (String str : codeCommands) {
            String[] strComponents = str.split(" ");

            if (strComponents[0].equals("class")) {
                classLine = true;
                if (classEncountered) {
                    vmFile.close();
                }
                classEncountered = true;
                String fileName = strComponents[1] + ".vm";

                vmFile = new FileWriter(fileName);

            } else if (!strComponents[0].equals("class")) {
                classLine = false;
            }

            if (classEncountered && vmFile != null && !classLine) {
                vmFile.write(str + "\n");
            }
        }

        vmFile.close();
    }
}
