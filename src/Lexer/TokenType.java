package Lexer;

/** Enumerator containing all reserved types of the JACK language */
public enum TokenType {keyword, id, num, stringLiteral, symb, op, eof}