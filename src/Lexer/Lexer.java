package Lexer;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * This class implements the lexical analyser (lexer) for
 * the JACK programming language. The lexer is responsible
 * for extracting tokens from an input source file.
 * */
public class Lexer{
    private int atToken;

    // container for line of the input file
    private ArrayList<String> stream;

    // container for each token found in the input stream
    private ArrayList<Token> tokens;

    // container for reserved keywords of JACK language
    LinkedList<String> reservedWords;

    // container for allowed single symbols of JACK language
    char[] allowedSymbols = {'(', ')', '{', '}', '[', ']', ',', ';', '.'};

    // container for allowed operators of JACK language
    char[] operators = {'+', '-', '/', '*', '&', '|', '~', '<', '>', '='};

    /* flag variables for comment traversal */

    // to indicate that multi line comment was encountered but not finalised
    private boolean longComment = false;

    // to indicate that * was encountered after multiline comment began.
    // If / is the preceeding symbol, the comment is terminatable.
    private boolean longCommentEndable = false;

    public Lexer(String fileName) {
        this.stream = null;
        this.atToken = 0;
        // first read and format file input stream
        try {
            parseFile(fileName);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        this.reservedWords = initReservedKeywords();
        this.tokens = new ArrayList<Token>();
        // then parse the extracted formatted stream to extract tokens
        parseTokens();
    }

    public void TestStream() {
        for (int i=0; i < stream.size(); i++) {
            System.out.println(stream.get(i));
        }
    }

    /* Consumes and returns the next token from the input stream */
    public Token GetNextToken() {

        try {
            Token nextToken = tokens.get(atToken);
            this.atToken++;
            return nextToken;
        }
        catch(Exception e){
            System.out.println("Attempt to access tokens beyond EOF");
            return null;
        }

    }

    /* Returns the next token from the input stream without consuming it*/
    public Token PeekNextToken() {

        try {
            return tokens.get(atToken);

        }
        catch(Exception e){
            System.out.println("Attempt to access tokens beyond EOF");
            return null;
        }

    }

    // method to initialise predetermined possible keywords for JACK language
    private LinkedList<String> initReservedKeywords() {

        LinkedList<String> words = new LinkedList<String>();
        words.add("class");
        words.add("constructor");
        words.add("method");
        words.add("function");
        words.add("int");
        words.add("boolean");
        words.add("char");
        words.add("void");
        words.add("var");
        words.add("static");
        words.add("field");
        words.add("let");
        words.add("do");
        words.add("if");
        words.add("else");
        words.add("while");
        words.add("return");
        words.add("true");
        words.add("false");
        words.add("null");
        words.add("this");

        return words;
    }

    /* Scans the given file and stores all stream of characters in memory.
     *  Trailing whitespaces and comments are removed from the stream before storing.*/
    private void parseFile(String fileName) throws IOException{
        // to store all lines of the input file
        ArrayList<String> stream = new ArrayList<String>();

        File file = new File(fileName);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);

        String line;
        while ((line = br.readLine()) != null) { // until EOF is encountered
            stream.add(formatLine(line));
        }

        this.stream = stream;
    }

    // Removes trailing whitespaces and comments.
    private String formatLine(String line){

        // indicate the index after which the characters in the line
        // should be discarded due to line comment
        int commentAfter = line.length() - 1;

        // flag for possible comment initialisation
        boolean slashFound = false;

        // commented sections are removed below
        for (int i = 0; i < line.length(); i++){
            char ch = line.charAt(i);

            if (!longComment && i == 0){ // ensures reinit variables when not continuing long comments
                commentAfter = line.length() - 1;
            }

            if (longComment){
                if (i == 0) { // multiline comment continues in the given new line
                    commentAfter = 0;
                }

                // end of line reached, remove commented section from this line
                if (i == line.length()-1) {
                    if (ch == '/' && longCommentEndable) {
                        longComment = false;
                    }
                    return removeCommentFromLine(line,commentAfter).trim();
                }

                // to indicate that normal processing of next characters can continue
                if (ch == '/' && longCommentEndable) {
                    // clears the flags
                    longComment = false;
                    longCommentEndable = false;
                    slashFound = false;
                    line = removeCommentFromLine(line,commentAfter,i);
                    // to ensure outer loop does not go out of range after modification,
                    // as well as further checks for multiline comments in the same line.
                    i = 0;

                    continue;
                }

                else if (ch == '*') {
                    longCommentEndable = true;
                }

                // last encountered * cannot be considered as terminal anymore
                else {
                    longCommentEndable = false;
                }
            }

            // double slash line comment encountered
            else if (ch == '/' && slashFound) {
                commentAfter = i-1;
                return removeCommentFromLine(line,commentAfter);
            }

            else if (ch == '/') {
                slashFound = true;
            }

            // indicate beginning of multiline comment
            else if (ch == '*' && slashFound) {
                longComment = true;
                slashFound = false;
                commentAfter = i-1;
            }

            // character was different from / ,hence clear flags
            else {
                slashFound = false;
            }
        }
        // finally trim the trailing whitespaces at the beginning and end of the line
       return line.trim();
    }

    // removes everything from the line after the encountered comment start
    private String removeCommentFromLine(String line, int comStartIndex) {
        if(comStartIndex == 0) {
            return "";
        }

        return line.substring(0,comStartIndex);
    }

    // removes partitioned section of a line, indicated by the beginning and end of a comment
    private String removeCommentFromLine(String line, int comStartIndex, int comEndIndex){
        String modifiedLine;

        if (comStartIndex != 0) { // the comment is internal in the line
            if (comEndIndex == line.length()-1) { // the comment reaches the end of the line
                modifiedLine = line.substring(0,comStartIndex);
            }
            else { // the comment ended before the end of line was encountered
                modifiedLine = line.substring(0,comStartIndex) + line.substring(comEndIndex+1);
            }
        }

        else { // line is commented form the beginning
            if (comEndIndex == line.length()-1){ // when the whole line is commented
                modifiedLine = "";
            }
            else {
                modifiedLine = line.substring(comEndIndex+1);
            }
        }

        return modifiedLine;
    }

    /* Extracts all tokens from the parsed input stream.
     *  They are stored for use on demand. */
    private void parseTokens(){

        // while there are remaining lines to extract tokens from
        for (int atLine = 0; atLine < stream.size(); atLine++){
            String line = stream.get(atLine);

            if (line == "" || line.length() == 0) { // empty line
                continue;
            }

            StringBuilder sb = new StringBuilder();

            int atChar = 0;
            char ch = line.charAt(0);

            // until the end of line is encountered
            while (ch != Character.MIN_VALUE){

                if ( ch == ' ') { // spaces do not represent tokens, hence can be skipped
                    atChar++;
                    ch = getNextChar(line, atChar);
                    continue;
                }

                if ( Character.isDigit(ch)) { // only numbers can start with a digit
                    sb.setLength(0);

                    sb.append(ch);
                    atChar++;
                    ch = getNextChar(line, atChar);

                    while ( Character.isDigit(ch) ) {
                        sb.append(ch);
                        atChar++;
                        ch = getNextChar(line, atChar);
                    }

                    tokens.add(new Token(TokenType.num, sb.toString(), atLine+1));
                    continue;
                }

                // the beginning of a string was encountered
                if ( ch == '"') {
                    sb.setLength(0);
                    sb.append(ch);
                    atChar++;
                    ch = getNextChar(line, atChar);
                    while ( ch != '"'){ // traverse until the end of string
                        // potentially the end of line could be reached before the end
                        // of string was encountered, in which case throw an error
                        if ( ch == Character.MIN_VALUE) {
                            Token stringToken = new Token(
                                TokenType.stringLiteral,
                                sb.toString(),atLine+1
                            );
                            stringToken.setError("String literal started but not terminated");
                            tokens.add(stringToken);
                            return; // parsing further is not necessary
                        }
                        // otherwise append the character to the lexeme being built
                        sb.append(ch);
                        atChar++;
                        ch = getNextChar(line,atChar);
                    }

                    // the " denoting the end of string should be added as well at the end of iteration
                    sb.append(ch);
                    tokens.add(new Token(TokenType.stringLiteral, sb.toString(), atLine+1));
                    atChar++;
                    ch = getNextChar(line,atChar);
                    continue;
                }

                // if a token starts with a letter it can be either a keyword or id
                if ( Character.isLetter(ch) || ch == '_') {
                    sb.setLength(0);
                    sb.append(ch);
                    atChar++;
                    ch = getNextChar(line,atChar);
                    while (ch == '_' ||
                            Character.isDigit(ch) ||
                                    Character.isLetter(ch) ) {
                        sb.append(ch);
                        atChar++;
                        ch = getNextChar(line,atChar);
                    }

                    String lexeme = sb.toString();
                    TokenType type = determineType(lexeme);
                    tokens.add(new Token(type,lexeme,atLine+1));
                    ch = getNextChar(line,atChar);
                    continue;
                }

                // checks if it belongs to the set of allowed symbols
                if (isSymbol(ch)) {
                    sb.setLength(0);
                    sb.append(ch);
                    tokens.add(new Token(TokenType.symb, sb.toString(), atLine+1));
                    atChar++;
                    ch = getNextChar(line,atChar);
                    continue;
                }

                // checks if it belongs to the set of allowed operators
                if (isOperator(ch)) {
                    sb.setLength(0);
                    sb.append(ch);
                    tokens.add(new Token(TokenType.op, sb.toString(), atLine+1));
                    atChar++;
                    ch = getNextChar(line,atChar);
                    continue;
                }

                // the end of line was found, the iteration can skip to the execution of next line
                if (ch == Character.MIN_VALUE) {
                    continue;
                }

                else { // invalid symbol was encountered when parsing
                    sb.setLength(0);
                    sb.append(ch);
                    Token unexpSymb = new Token(TokenType.symb, sb.toString(), atLine+1);
                    unexpSymb.setError("Encountered unknown symbol");
                    tokens.add(unexpSymb);
                    return;
                }
            }
        }

        // lastly add a token indicating that the end of file was reached

        if (tokens.size() != 0) {
            int last = tokens.size()-1;
            tokens.add(new Token(TokenType.eof, "EOF", tokens.get(last).getLine()));
        }
        else {
            tokens.add(new Token(TokenType.eof, "EOF", -1));
        }
    }

    // helper method to return an indicator if the end of word is attempted to be parsed
    // @returns //u0000 in the above mentioned case OR the character at specified index
    private char getNextChar(String word, int at) {
        if (at == word.length()) {
            return Character.MIN_VALUE;
        }

        else{
            return word.charAt(at);
        }
    }

    // helper method, which determines if a character belongs to set of allowed symbols
    private boolean isSymbol(char ch) {
        for(int i=0; i < allowedSymbols.length; i++) {
            if (ch == allowedSymbols[i]) {
                return true;
            }
        }

        return false;
    }

    // helper method, which determines if a character belongs to a set of allowed operators
    private boolean isOperator(char ch) {
        for(int i=0; i < operators.length; i++) {
            if (ch == operators[i]) {
                return true;
            }
        }

        return false;
    }

    // utility method to determine if a lexeme is a keyword or identifier
    private TokenType determineType(String lexeme) {

        for (int i=0; i < reservedWords.size(); i++) {
            if (lexeme.equals(reservedWords.get(i))) {
                return TokenType.keyword;
            }
        }

        return TokenType.id;
    }

}