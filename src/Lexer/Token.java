package Lexer;
import java.io.*;

/** Container class for the tokens, referenced by the compiler */
public class Token{

    private TokenType type;
    private String lexeme;
    private int line;
    private boolean error;
    private String errorMessage;

    public Token(TokenType type, String lexeme, int line) {
        this.type = type;
        this.lexeme = lexeme;

        this.line = line;
        this.error = false;
        this.errorMessage = "";
    }

    /** Indicates error and displays custom supplied message, as well as
     *  the lexeme at which it was encountered and the according line number*/
    public void setError(String errorMessage) {
        this.error = true;
        this.errorMessage = errorMessage + " " + this.lexeme + " at line " + this.line;
    }

    public TokenType getType() {
        return this.type;
    }

    public String getLexeme() {
        return this.lexeme;
    }

    public int getLine() {
        return this.line;
    }

    public boolean hasError() {
        return this.error;
    }

    // Displays the essence of a token for debugging and informative purposes
    @Override
    public String toString() {
        if (!error) {
            return this.type + " " + this.lexeme + " " + this.line;
        }

        return errorMessage;
    }
}